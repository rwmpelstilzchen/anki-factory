# 🏭 Maja’s Anki Deck Factory

👋 Avĕ! Welcome to the factory!  
🌍 I’m an avid user of [Anki](https://apps.ankiweb.net/), because it helps me expand my world.  
🗃️ Over the years I created many decks, but unfortunately I failed to share them in one easily available place; [this Git repository](https://app.radicle.xyz/nodes/seed.radicle.garden/rad:z2PAWAdhwvbpiYwm86bArS89NvhoK) aims at fixing that, one deck at a time. It is forged in [Radicle](https://radicle.xyz/), a new peer-to-peer distributed forge.  
⚙️  I like to produce decks programmatically from source files, and avoid manual data insertion in the Anki GUI; hence the name, *Factory*. Currently I prefer [genanki-rs](https://crates.io/crates/genanki-rs) for 🦀 Rust, but previously I used [genanki](https://github.com/kerrickstaley/genanki) for 🐍 Python or simply outputting 📄 CSV files manually.  
🝓  Working with text files makes the choice of a Git repository the natural choice for sharing.  
🤝 Feel free to open [an issue](https://app.radicle.xyz/nodes/seed.radicle.garden/rad:z2PAWAdhwvbpiYwm86bArS89NvhoK/issues) or [a patch](https://app.radicle.xyz/nodes/seed.radicle.garden/rad:z2PAWAdhwvbpiYwm86bArS89NvhoK/patches), or [contact me](https://me.digitalwords.net/) about anything 🙂


## 🌐 What’s publicly available so far?

⭐ All of the decks shared on AnkiWeb are concentrated [here](https://ankiweb.net/shared/by-author/708121003).

| Language | Name | Description | `.apkg` | AnkiWeb | Created | Updated |
| --- | --- | --- | --- | --- | --- | ---|
| Multiple | [Tatoeba Project](https://digitalwords.net/anki/tatoeba-audio/index.eng.html) | All sentences with audio | [📥](https://digitalwords.net/anki/tatoeba-audio/index.eng.html#sec:precompiled:download) | [⭐](https://ankiweb.net/shared/decks?search=sentences%20with%20audio%20from%20Tatoeba) | 2019-12-20 | 2019-12-20 |
| | [😜 Gitmoji](./zxx/gitmoji/) | Memorise an emoji guide for commit messages | [📥](./zxx/gitmoji/gitmoji.apkg) | [⭐](https://ankiweb.net/shared/info/813391103) | 2024-10-05 | 2024-10-05 |
