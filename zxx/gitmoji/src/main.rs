use serde::{Deserialize, Serialize};
use genanki_rs::{Deck, Error, Field, Model, Note, Template};

#[derive(Serialize, Deserialize)]
struct Gitmoji {
    emoji: String,
    entity: String,
    code: String,
    description: String,
    name: String,
    semver: Option<String>,
}

fn main() -> Result<(), Error> {
    let mut deck= Deck::new(
        2127384093, // hard-coded; chosen at random (`random.randrange(1 << 30, 1 << 31)` in Python)
        "😜 Gitmoji",
        r#"<tt>🧑‍💻 Learn <a href="https://gitmoji.dev/">Gitmoji</a> (an emoji guide for commit messages) by heart.<br><br>Reflects <a href="https://github.com/carloscuesta/gitmoji/blob/d979d9e852cadbd5d2a5f93b8e42812328053cca/packages/gitmojis/src/gitmojis.json">gitmojis.json as of commit d979d9e</a> (2023-10-10), which is the most up-to-date version at the time this package was generated (2024-10-05).<br><br>🏭 This deck has been forged at <a href="https://gitlab.com/rwmpelstilzchen/anki-factory/">Maja’s Anki deck factory</a>. For more decks see <a href="https://ankiweb.net/shared/by-author/1373615545">this page</a> on AnkiWeb and <a href="https://gitlab.com/rwmpelstilzchen/anki-factory/">this Git repository</a> on GitLab.</tt>"#
    );

    let model = Model::new(
        1252987233, // hard-coded; chosen at random (`random.randrange(1 << 30, 1 << 31)` in Python)
        "😜 Gitmoji",
        vec![
            Field::new("😜 emoji"),
            Field::new("🧑‍💻 code"),
            Field::new("💡 description"),
            Field::new("🔖 semver"),
        ],
        vec![
            Template::new("👁️ Gitmoji recognition")
                .qfmt("<p>{{😜 emoji}} <code>{{🧑‍💻 code}}</code></p>")
                .afmt(r#"{{FrontSide}}<hr id="answer"><p>{{💡 description}}</p>"#),
            Template::new("⌨️ Gitmoji production")
                .qfmt("<p>{{💡 description}}</p>")
                .afmt(r#"{{FrontSide}}<hr id="answer"><p>{{😜 emoji}} <code>{{🧑‍💻 code}}</code></p>"#),
            Template::new("🔖 SemVer")
                .qfmt("<p>Effect on SemVer?</p><p>{{😜 emoji}} <code>{{🧑‍💻 code}}</code><br>{{💡 description}}</p>")
                .afmt(r#"{{FrontSide}}<hr id="answer"><p>{{#🔖 semver}}Yes, bump <code>{{🔖 semver}}</code>.{{/🔖 semver}}{{^🔖 semver}}No.{{/🔖 semver}}</p>"#),
        ]
    );

    let gitmojis_file = std::fs::File::open("gitmojis.json").expect("🥅 Could not open file.");
    let gitmojis: Vec<Gitmoji> = serde_json::from_reader(gitmojis_file).expect("🥅 Could not read values.");
    for gitmoji in gitmojis {
        // println!("{}", gitmoji.semver.unwrap_or("".to_string()));
        deck.add_note(Note::new(model.clone(), vec![
            &gitmoji.emoji,
            &gitmoji.code,
            &gitmoji.description,
            &gitmoji.semver.unwrap_or("".to_string())
        ])?);
    }

    deck.write_to_file("gitmoji.apkg")?;
    Ok(())
}
