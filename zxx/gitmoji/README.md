# 😜 Gitmoji Anki deck

🧑‍💻 [Gitmoji](https://gitmoji.dev/) is an emoji guide for commit messages, aiming at serving as a standardisation cheatsheet. Using emoji on commit messages provides an easy way to identify the purpose or intention of a commit just from looking at the emoji used.

🗃️ This deck teaches all the emoji in the Gitmoji database:

- 👁️ Learn to recognise the emoji and understand what they mean.
- ⌨️ Learn what emoji to use for common scenarios.
- 🔖 Learn the when to bump the [SemVer](https://semver.org/) version.

👉 [Gitmemoji](https://gitmemoji.imbios.dev/) has a similar purpose, but it has no SRS, doesn’t teach to recognise emoji, doesn’t refer to versioning, and currently is out-of-date.

---

🏭 This deck has been manufactured at my Anki deck factory. For more decks see [this page](https://ankiweb.net/shared/by-author/1373615545) on AnkiWeb and the factory’s [Git repository](https://app.radicle.xyz/nodes/seed.radicle.garden/rad:z2PAWAdhwvbpiYwm86bArS89NvhoK) on the [Radicle](https://radicle.xyz/) peer-to-peer distributed forge.
